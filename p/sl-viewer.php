<!doctype html>
<html>
<head>
    <title id="title">~IRC~</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="/p/css/style-base.css">
    <link rel="stylesheet" type="text/css" href="/p/css/style-sl-viewer.css">
</head>

<script src="/node_modules/socket.io/node_modules/socket.io-client/socket.io.js"></script>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="/p/js/core.js"></script>

<body>
    <div id="body-wrapper">
        <div id="sidebar">
            <p>Online</p>
            <ul id="users_online">

            </ul>
        </div>

        <ul id="chat-content">
            <!--
                    MESSAGES
             -->
        </ul>

        <div id="login" class="login_style">
            <h3>Login</h3>
            <div id="login-wrapper2">
                <form id="form_login" action="">
                    <label for="login_username"></label>
                    <input id="login_username" placeholder="Username" minlength="2" maxlength="15" form="form_login" autofocus>
                    <button form="form_login">Connect</button>
                </form>
            </div>
        </div>

        <div id="message-wrapper">
            <form id="form_send_message" action="">
                <label for="submit_message"></label>
                <input id="submit_message" autocomplete="off" maxlength="999" autofocus>
            </form>
        </div>
    </div>
    <audio id="boop" src="/p/sounds/pop.mp3" preload="auto"></audio>
</body>
</html>