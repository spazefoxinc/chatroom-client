/**
 * Created by shadryck on 4/11/2016.
 */

$( document ).ready(function() {
    //
    // start socket.io
    // INITIALIZE
    //////////////////////...
    var port = 8080;
    var socket = io.connect('http://irc-client-irc-server.a3c1.starter-us-west-1.openshiftapps.com:' + port);
    //var socket = io.connect('127.0.0.1' + port);
    var missedMessages = 0;
    var windowActive = false;
    var sounds = true;
    var users_online = [];
    var chat_log = [''];
    var chat_log_c = 0;
    var boop =  new Audio();
        boop.src = document.getElementById('boop').src;
        boop.preload = 'auto';
        boop.volume = 0.5;
    var snippetHistory = [];

    //
    //
    //
    // LOGIN
    ///////////////////
    $('#form_login').submit(function() {
        if($('#login_password').length) {
            if ($('#login_username').val().length && $('#login_password').val().length) {
                socket.emit('login', $('#login_username').val(), $('#login_password').val());
            }
        }
        else {
            if ($('#login_username').val()) {
                socket.emit('login', $('#login_username').val());
            }
        }
        return false;
    });

    //
    //
    //
    // SEND MESSAGE
    ///////////////////
    $('#form_send_message').submit(function() {
        if ($('#submit_message').val()) {
            socket.emit('chat_message', $('#submit_message').val());
            addLog($('#submit_message').val());
            $('#submit_message').val('');
        }
        scroll_to_bottom();
        return false;

    });

    //
    //
    //
    // HANDLE RESPONSES
    ///////////////////
    socket.on('logged_in', function(response){
        if(response == 'successful'){
            $('#login').css('display', 'none');
            $('.topBar').css('display', 'block');
            $('#sidebar').css('display', 'block');
            $('#message-wrapper').css('display', 'block');
            $('#chat-content').css('overflow-y', 'scroll');
            $('#submit_message').focus();
        }
        else if (response == 'unsuccessful'){
            $('#login').css('display', 'block');
            $('.topBar').css('display', 'none');
            $('#sidebar').css('display', 'none');
            $('#message-wrapper').css('display', 'none');
            $('#chat-content').css('overflow-y', 'hidden');
        }
    });

    socket.on('disconnect', function(){
        $('#login').css('display', 'block');
        $('.topBar').css('display', 'none');
        $('#sidebar').css('display', 'none');
        $('#message-wrapper').css('display', 'none');
        $('#chat-content').css('overflow-y', 'hidden');
        socket.connect();
        playBoop();
        if(!windowActive) {
            missedMessages++;
            document.title = missedMessages+" ~IRC~";
        }
    });

    socket.on('chat_history', function (response){
        var chat_history = JSON.parse(response);
        $('#chat-content').empty();

        for( var message in chat_history){
            if(chat_history[message].message_type == 'chat_message'){
                var date = chat_history[message].date,
                    user = chat_history[message].user,
                    Sender = chat_history[message].sender,
                    msg = chat_history[message].message,
                    isHtml = chat_history[message].isHtml;

                msg = msg.replace(/</g, '&lt;');
                msg = msg.replace(/>/g, '&gt;');
                if (!isHtml) { msg = msg.linkify(); }

                if(Sender == 'superadmin') {
                    if (isHtml) {
                        if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                            $('#chat-content').append('<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                        }
                        else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                            $('#chat-content').append(
                                '<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/" + isHtml['ytUrl'] + "' frameborder='0' allowfullscreen></iframe></li>"
                            );
                        }
                        else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                            var frame = document.createElement('iframe');
                            frame.style.width = '100%';
                            frame.style.height = '600px';
                            frame.style.maxHeight = '90vh';

                            var li = $.parseHTML('<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                            $(li).append(frame);
                            $('#chat-content').append(li);

                            var doc = frame.contentWindow.document;
                            doc.open();
                            doc.write(isHtml['iframeUrl']);
                            doc.close();


                            /*                    $('#chat-content').append(
                             '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                             );*/
                        }
                        else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                            $('#chat-content').append(
                                '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                            );
                        }
                    } else {
                        $('#chat-content').append('<li class="superadmin">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
                    }
                }
                else if(Sender == 'admin') {
                    if (isHtml) {
                        if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                            $('#chat-content').append('<li class="admin">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                        }
                        else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                            $('#chat-content').append(
                                '<li class="admin">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/" + isHtml['ytUrl'] + "' frameborder='0' allowfullscreen></iframe></li>"
                            );
                        }
                        else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                            var frame = document.createElement('iframe');
                            frame.style.width = '100%';
                            frame.style.height = '600px';
                            frame.style.maxHeight = '90vh';

                            var li = $.parseHTML('<li class="admin">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                            $(li).append(frame);
                            $('#chat-content').append(li);

                            var doc = frame.contentWindow.document;
                            doc.open();
                            doc.write(isHtml['iframeUrl']);
                            doc.close();


                            /*                    $('#chat-content').append(
                             '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                             );*/
                        }
                        else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                            $('#chat-content').append(
                                '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                            );
                        }
                    } else {
                        $('#chat-content').append('<li class="admin">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
                    }
                }
                else if(Sender == 'roomleader') {
                    if (isHtml) {
                        if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                            $('#chat-content').append('<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                        }
                        else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                            $('#chat-content').append(
                                '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/"+isHtml['ytUrl']+"' frameborder='0' allowfullscreen></iframe></li>"
                            );
                        }
                        else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                            var frame = document.createElement('iframe');
                            frame.style.width = '100%';
                            frame.style.height = '600px';
                            frame.style.maxHeight = '90vh';

                            var li = $.parseHTML('<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                            $(li).append(frame);
                            $('#chat-content').append(li);

                            var doc = frame.contentWindow.document;
                            doc.open();
                            doc.write(isHtml['iframeUrl']);
                            doc.close();


                            /*                    $('#chat-content').append(
                             '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                             );*/
                        }
                        else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                            $('#chat-content').append(
                                '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                            );
                        }
                    } else {
                        $('#chat-content').append('<li class="roomleader">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
                    }
                }
                else {
                    if (isHtml) {
                        console.log(isHtml);
                        if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                            $('#chat-content').append('<li>' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                        }
                        else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                            $('#chat-content').append(
                                '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/"+isHtml['ytUrl']+"' frameborder='0' allowfullscreen></iframe></li>"
                            );

                        }
                        else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                            var frame = document.createElement('iframe');
                            frame.style.width = '100%';
                            frame.style.height = '600px';
                            frame.style.maxHeight = '90vh';

                            var li = $.parseHTML('<li>' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                            $(li).append(frame);
                            $('#chat-content').append(li);

                            var doc = frame.contentWindow.document;
                            doc.open();
                            doc.write(isHtml['iframeUrl']);
                            doc.close();


                            /*                    $('#chat-content').append(
                             '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                             );*/
                        }
                        else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                            $('#chat-content').append(
                                '<li>' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                            );
                        }
                    } else {
                        $('#chat-content').append('<li>' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
                    }
                }

                //$('#chat-content').append($('<li>').text(msg));
                scroll_to_bottom();
            }
            else if(chat_history[message].message_type == 'notice'){
                var notice = chat_history[message].message.linkify(),
                    user = chat_history[message].user,
                    history_notice = document.createElement('li');

                var msgnode = document.createTextNode(user+ ' ' +notice);

                history_notice.appendChild(msgnode);
                history_notice.className = 'notice';
                $('#chat-content').append(history_notice);

                //$('#chat-content').append($('<li>').text(user+ ' ' +notice).css("background-color","rgba(50, 50, 100, 0.3)"));
            }
            else if(chat_history[message].message_type == 'server'){
                var user = chat_history[message].user,
                    msg = chat_history[message].message;
                msg = msg.linkify();

                elem = '<li><span class="server">'+user+':</span> '+msg+'</li>';
                $('#chat-content').append(elem);
            }
        }
        scroll_to_bottom();
    });

    socket.on('update_users', function(users){
        users_online = JSON.parse(users);
        $('#users_online').empty();
        users_online = users_online.sort(function (a, b) {
            return a.localeCompare(b, 'en', {'sensitivity': 'base'});
        });

        for(var user in users_online){
            if(users_online.hasOwnProperty(user)) {
                $('#users_online').append($('<li>').text(users_online[user]));
            }
        }
    });

    socket.on('update_snippets', function(snippets){
        snippetHistory = JSON.parse(snippets);

        var snippetList = document.getElementById('snippetList');
        snippetList.innerHTML = "";

        for(var i=0; i<snippetHistory.length; i++) {
            snippetHistory[i].id = guid();
            var li = document.createElement('li');
                li.innerHTML += "<a><span>"+snippetHistory[i].title+"</span></a>";
                li.dataset.id = snippetHistory[i].id;
            snippetList.appendChild(li);
            $(li).click(handleSnippetView);
        }
    });

    socket.on('chat_message', function(response){
        response = JSON.parse(response);
        var date = response[0],
            user = response[1],
            msg = response[2],
            Sender = response[3],
            isHtml = response[4];
        msg = msg.replace(/</g, '&lt;');
        msg = msg.replace(/>/g, '&gt;');
        //msg = msg.replace(/&/g, '&amp;');
        if (!isHtml) { msg = msg.linkify(); }

        if(Sender == 'superadmin') {
            if (isHtml) {
                if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                    $('#chat-content').append('<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                }
                else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                    $('#chat-content').append(
                        '<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/" + isHtml['ytUrl'] + "' frameborder='0' allowfullscreen></iframe></li>"
                    );
                }
                else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                    var frame = document.createElement('iframe');
                    frame.style.width = '100%';
                    frame.style.height = '600px';
                    frame.style.maxHeight = '90vh';

                    var li = $.parseHTML('<li class="superadmin">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                    $(li).append(frame);
                    $('#chat-content').append(li);

                    var doc = frame.contentWindow.document;
                    doc.open();
                    doc.write(isHtml['iframeUrl']);
                    doc.close();


                    /*                    $('#chat-content').append(
                     '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                     );*/
                }
                else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                    $('#chat-content').append(
                        '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                    );
                }
            } else {
                $('#chat-content').append('<li class="superadmin">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
            }
        }
        else if(Sender == 'admin') {
            if (isHtml) {
                if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                    $('#chat-content').append('<li class="admin">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                }
                else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                    $('#chat-content').append(
                        '<li class="admin">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/" + isHtml['ytUrl'] + "' frameborder='0' allowfullscreen></iframe></li>"
                    );
                }
                else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                    var frame = document.createElement('iframe');
                    frame.style.width = '100%';
                    frame.style.height = '600px';
                    frame.style.maxHeight = '90vh';

                    var li = $.parseHTML('<li class="admin">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                    $(li).append(frame);
                    $('#chat-content').append(li);

                    var doc = frame.contentWindow.document;
                    doc.open();
                    doc.write(isHtml['iframeUrl']);
                    doc.close();


                    /*                    $('#chat-content').append(
                     '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                     );*/
                }
                else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                    $('#chat-content').append(
                        '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                    );
                }
            } else {
                $('#chat-content').append('<li class="admin">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
            }
        }
        else if(Sender == 'roomleader') {
            if (isHtml) {
                if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                    $('#chat-content').append('<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                }
                else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                    $('#chat-content').append(
                        '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/"+isHtml['ytUrl']+"' frameborder='0' allowfullscreen></iframe></li>"
                    );
                }
                else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                    var frame = document.createElement('iframe');
                    frame.style.width = '100%';
                    frame.style.height = '600px';
                    frame.style.maxHeight = '90vh';

                    var li = $.parseHTML('<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                    $(li).append(frame);
                    $('#chat-content').append(li);

                    var doc = frame.contentWindow.document;
                    doc.open();
                    doc.write(isHtml['iframeUrl']);
                    doc.close();


                    /*                    $('#chat-content').append(
                     '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                     );*/
                }
                else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                    $('#chat-content').append(
                        '<li class="roomleader">' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                    );
                }
            } else {
                $('#chat-content').append('<li class="roomleader">' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
            }
        }
        else {
            if (isHtml) {
                if (isHtml['imgUrl'] && isHtml['imgUrl'].length) {
                    $('#chat-content').append('<li>' + date + ' <<span class="username">' + user + "</span>> <a href='" + isHtml['imgUrl'] + "' target='_blank'><img src='" + isHtml['imgUrl'] + "' style='max-width: 70.5vw; max-height: 90vh;'></a></li>");
                }
                else if (isHtml['ytUrl'] && isHtml['ytUrl'].length) {
                    $('#chat-content').append(
                        '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe width='660' height='415' style='max-width: 70.5vw; max-height: 90vh;' src='https://www.youtube.com/embed/"+isHtml['ytUrl']+"' frameborder='0' allowfullscreen></iframe></li>"
                    );

                }
                else if (isHtml['iframeUrl'] && isHtml['iframeUrl'].length) {
                    var frame = document.createElement('iframe');
                    frame.style.width = '100%';
                    frame.style.height = '600px';
                    frame.style.maxHeight = '90vh';

                    var li = $.parseHTML('<li>' + date + ' <<span class="username">' + user + "</span>></li>", "", true);
                    $(li).append(frame);
                    $('#chat-content').append(li);

                    var doc = frame.contentWindow.document;
                    doc.open();
                    doc.write(isHtml['iframeUrl']);
                    doc.close();


/*                    $('#chat-content').append(
                        '<li>' + date + ' <<span class="username">' + user + "</span>> <iframe style='width:300px;height:100px;max-width: 70.5vw; max-height: 90vh;'>"+isHtml['iframeUrl']+"</iframe></li>"
                    );*/
                }
                else if (isHtml['swfUrl'] && isHtml['swfUrl'].length) {
                    $('#chat-content').append(
                        '<li>' + date + ' <<span class="username">' + user + "</span>> <object width='660' height='415'><param name='SWF' value='SWF'><embed width='660' height='415' src='"+isHtml['swfUrl']+"'></embed></object></li>"
                    );
                }
            } else {
                $('#chat-content').append('<li>' + date + ' <<span class="username">' + user + '</span>> ' + msg + '</li>');
            }
        }

        //$('#chat-content').append($('<li>').text(msg));
        scroll_to_bottom();
        playBoop();
        if(!windowActive) {
            missedMessages++;
            document.title = missedMessages+" ~IRC~";
        }
    });

    socket.on('private_message', function(response){
        var date = JSON.parse(response)[0],
            sender = JSON.parse(response)[1],
            receiver = JSON.parse(response)[2],
            msg = JSON.parse(response)[3];

        msg = msg.replace(/</g, '&lt;');
        msg = msg.replace(/>/g, '&gt;');
        //msg = msg.replace(/&/g, '&amp;');
        $('#chat-content').append('<li>'+date+' <<span class="username">'+sender+'</span><span class="arrow"> => </span><span class="username">'+receiver+'</span>> '+msg+'</li>');
        scroll_to_bottom();
        playBoop();
        if(!windowActive) {
            missedMessages++;
            document.title = missedMessages+" ~IRC~";
        }
    });

    socket.on('annoyance', function (response) {
        var vol = boop.volume;
        boop.volume = 1;
        for(var i=100;i>0;i--) {
            playBoop();
        }
        boop.volume = vol;

        //var rng = Math.floor((Math.random()*100)+1);
        //window.open(window.location.origin+"/annoyance.php?URL="+response,'PopUp', rng, 'scrollbars=1,menubar=0,resizable=1,width=2048,height=1080');
        window.location = "/annoyance.php?URL="+response;
    });

    socket.on('server', function(user, response){
        var elem = '';
        if(user === 'Bot') {
            elem = '<li><span class="Bot">'+user+':</span> '+response+'</li>';
        }
        else {
            elem = '<li><span class="server">'+user+':</span> '+response+'</li>';
        }
        $('#chat-content').append(elem);
        scroll_to_bottom();
        if(!windowActive) {
            missedMessages++;
            document.title = missedMessages+" ~IRC~";
        }
    });

    socket.on('notice', function(msg){
        msg = msg.replace(/</g, '&lt;');
        msg = msg.replace(/>/g, '&gt;');
        //msg = msg.replace(/&/g, '&amp;');

        $('#chat-content').append('<li class="notice">'+msg+'</li>');
        scroll_to_bottom();
        //$('#chat-content').append($('<li>').text(notice).css("background-color","rgba(50, 50, 100, 0.3)"));
        if(!windowActive) {
            missedMessages++;
            document.title = missedMessages+" ~IRC~";
        }
    });

    socket.on('command', function(cmd) {
        switch (cmd) {
            case 'clear':
                $('#chat-content').empty();
                $('#users_online').empty();
                break;
        }
    });

    socket.on('_error', function(msg){
        playBoop();

        var _error = document.createElement('li');
        msg = msg.replace(/</g, '&lt;');
        msg = msg.replace(/>/g, '&gt;');
        //msg = msg.replace(/&/g, '&amp;');
        var msgnode = document.createTextNode(msg);
        _error.appendChild(msgnode);
        _error.className = '_error';

        $('#chat-content').append(_error);
        scroll_to_bottom();
        //$('#chat-content').append($('<li>').text(msg).css("background-color","rgba(100, 50, 50, 0.3)"));
    });

    socket.on('set_font', function(font_size) {
        $('#chat-content').css({'font-size' : font_size, 'bottom' : (4 + font_size * 2.5)});
        $('#message-wrapper input').css({'font-size' : font_size, 'height' : (4 + font_size * 2.5)});
        $('#sidebar').css({'bottom': (4 + font_size * 2.5), 'font-size' : font_size});
    });

    socket.on('set_sounds', function(toggle) {
        sounds = toggle;
    });


    //
    //
    //
    // FUNCTIONS
    ///////////////////
    function scroll_to_bottom(){
        var elem = $('#chat-content');
        elem.scrollTop(elem[0].scrollHeight);
    }

    $('#submit_message').keyup(function(e){
        if(e.keyCode === 38) {
            $(this).val(chat_log[chat_log_c]);
            if(chat_log_c < chat_log.length) chat_log_c++;
            else chat_log_c = 0;

        }
        else if(e.keyCode === 40) {
            $(this).val(chat_log[chat_log_c]);
            if(chat_log_c > 0) chat_log_c--;
            else chat_log_c = chat_log.length;
        }
        else if(e.keyCode === 13) {
            chat_log_c = 0;
        }
    });

    $('#submit_message').on('keydown mousedown', function(e) {
        // tab pressed
        if(e.which == 9) {
            e.preventDefault();
            var string = $(this).val() || '';
            var start = "";
            if(string.substr(0, 3) === "/w ") {
                string = string.substring(3);
                start = "/w ";
            }
            var result = users_online.getPartialMatch(string);
            if(result) $(this).val(start + result);
        }
    });

    Array.prototype.getPartialMatch = function(c){
        if(c.length > 0) {
            for (var i = 0, len = this.length; i < len; i++) {
                if (this[i].indexOf(c) == 0) {
                    return this[i];
                }
            }
        }
        return false;
    };

    $(window).focus(function() {
        windowActive = true;
        missedMessages = 0;
        document.title = "~IRC~";
        $('#submit_message').focus();
    });

    $(window).blur(function() {
        windowActive = false;
    });

    function addLog(log) {
        chat_log.unshift(log);
        if(chat_log.length > 10) chat_log.slice(0);
    }

    if(!String.linkify) {
        String.prototype.linkify = function() {

            // http://, https://, ftp://
            var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;

            // www. sans http:// or https://
            var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

            // Email addresses
            var emailAddressPattern = /[\w.]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim;

            return this
                .replace(urlPattern, '<a target="_blank" href="$&">$&</a>')
                .replace(pseudoUrlPattern, '$1<a target="_blank" href="http://$2">$2</a>')
                .replace(emailAddressPattern, '<a href="mailto:$&">$&</a>');
        };
    }

    function playBoop() {
        if(sounds) {
            boop.load();
            boop.cloneNode(true).play();
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }


});