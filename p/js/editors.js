/**
 * Created by Anonymous on 5/2/2017.
 */

$(document).ready(function () {
    var editor = ace.edit('editor');
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode('ace/mode/javascript');
    editor.getSession().setTabSize(4);
    editor.getSession().setUseSoftTabs(true);

    var editorPreview = ace.edit('editor-preview');
    editorPreview.setTheme("ace/theme/monokai");
    editorPreview.getSession().setMode('ace/mode/javascript');
    editorPreview.setOptions({
        readOnly: true,
        highlightActiveLine: false,
        highlightGutterLine: false
    });
    editorPreview.renderer.$cursorLayer.element.style.opacity = 0;

    $('#codeTheme').change(function () {
        editor.setTheme("ace/theme/" + this.value);
    });
    $('#codeMode').change(function () {
        editor.getSession().setMode("ace/mode/" + this.value);
    });

    $('#topBar_Code').click(handleView);
    $('#topBar_IRC').click(handleView);
    $('#addNewSnippetViewButton').click(handleCodeView);
    $('#addSnippetCancel').click(handleCodeView);
    $('#snippetPreviewGoBack').click(handleCodeView);

    $('#addNewSnippetForm').submit(function () {
        var y = $('#codeSnippetTitle'),
            x = $('#editor');

        if (editor.getValue().length && y.val().length) {
            socket.emit('add_code_snippet', y.val(), editor.getValue(), $('#codeTheme').val(), $('#codeMode').val());
            $('.activeCodeView').removeClass('activeCodeView');
            $('.snippetsListView').addClass('activeCodeView');
        }
        return false;
    });

    function handleView() {
        $('.activeView').removeClass('activeView');

        var codeView = $('#topBar_Code')[0],  _this = $(this)[0];

        if (_this == codeView) {
            $('#codeView').addClass('activeView');
        }
    }

    function handleCodeView() {
        $('.activeCodeView').removeClass('activeCodeView');

        var a = $('#addNewSnippetViewButton')[0],
            b = $('#addSnippetCancel')[0],
            c = $('#snippetPreviewGoBack')[0],
            _this = $(this)[0];

        if (_this == a) {
            $('.addNewSnippetView').addClass('activeCodeView');
        }
        else if (_this == b) {
            $('.snippetsListView').addClass('activeCodeView');
        }
        else if (_this == c) {
            $('.snippetsListView').addClass('activeCodeView');
        }
    }

    function handleSnippetView() {
        $('.activeCodeView').removeClass('activeCodeView');
        $('#snippetPreviewView').addClass('activeCodeView');

        var id = $(this).data('id');
        var snippet = $.grep(snippetHistory, function(e){ return e.id == id; })[0];
        document.getElementById('snippetPreviewTitle').innerHTML = snippet.title;
        editorPreview.session.setValue(snippet.content);
        editorPreview.setTheme("ace/theme/"+snippet.theme);
        editorPreview.getSession().setMode("ace/mode/"+snippet.mode);
    }
});