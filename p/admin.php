<!doctype html>
<html>
<head>
    <title>~IRC~</title>
    <link rel="stylesheet" type="text/css" href="/p/css/style-base.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.6/ace.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script src="/node_modules/socket.io/node_modules/socket.io-client/socket.io.js"></script>
    <script src="/p/js/core.js"></script>
    <script src="/p/js/editors.js"></script>
</head>

<body>
    <div id="body-wrapper">
        <div class="topBar">
            <div class="item" id="topBar_IRC"><a>IRC</a></div>
            <div class="item" id="topBar_Code"><a>Code</a></div>
        </div>

        <div id="codeView" class="">
            <div class="addNewSnippetView">
                <form action="" id="addNewSnippetForm">
                    <input form="addNewSnippetForm" type="submit">
                    <button id="addSnippetCancel">Cancel</button>
                    <input form="addNewSnippetForm" id="codeSnippetTitle" type="text" placeholder="Title" autofocus>

                    <select id="codeMode" size="1">
                        <option value="abap">abap</option>
                        <option value="actionscript">actionscript</option>
                        <option value="ada">ada</option>
                        <option value="asciidoc">asciidoc</option>
                        <option value="assembly_x86">assembly_x86</option>
                        <option value="autohotkey">autohotkey</option>
                        <option value="batchfile">batchfile</option>
                        <option value="c9search">c9search</option>
                        <option value="c_cpp">c_cpp</option>
                        <option value="clojure">clojure</option>
                        <option value="cobol">cobol</option>
                        <option value="coffee">coffee</option>
                        <option value="coldfusion">coldfusion</option>
                        <option value="csharp">csharp</option>
                        <option value="css">css</option>
                        <option value="curly">curly</option>
                        <option value="d">d</option>
                        <option value="dart">dart</option>
                        <option value="diff">diff</option>
                        <option value="django">django</option>
                        <option value="dot">dot</option>
                        <option value="ejs">ejs</option>
                        <option value="erlang">erlang</option>
                        <option value="forth">forth</option>
                        <option value="ftl">ftl</option>
                        <option value="glsl">glsl</option>
                        <option value="golang">golang</option>
                        <option value="groovy">groovy</option>
                        <option value="haml">haml</option>
                        <option value="handlebars">handlebars</option>
                        <option value="haskell">haskell</option>
                        <option value="haxe">haxe</option>
                        <option value="html">html</option>
                        <option value="html_ruby">html_ruby</option>
                        <option value="ini">ini</option>
                        <option value="jade">jade</option>
                        <option value="java">java</option>
                        <option value="javascript" selected>javascript</option>
                        <option value="json">json</option>
                        <option value="jsoniq">jsoniq</option>
                        <option value="jsp">jsp</option>
                        <option value="jsx">jsx</option>
                        <option value="julia">julia</option>
                        <option value="latex">latex</option>
                        <option value="less">less</option>
                        <option value="liquid">liquid</option>
                        <option value="lisp">lisp</option>
                        <option value="livescript">livescript</option>
                        <option value="logiql">logiql</option>
                        <option value="lsl">lsl</option>
                        <option value="lua">lua</option>
                        <option value="luapage">luapage</option>
                        <option value="lucene">lucene</option>
                        <option value="makefile">makefile</option>
                        <option value="markdown">markdown</option>
                        <option value="matlab">matlab</option>
                        <option value="mushcode">mushcode</option>
                        <option value="mushcode_high_rules">mushcode_high_rules</option>
                        <option value="mysql">mysql</option>
                        <option value="objectivec">objectivec</option>
                        <option value="ocaml">ocaml</option>
                        <option value="pascal">pascal</option>
                        <option value="perl">perl</option>
                        <option value="pgsql">pgsql</option>
                        <option value="php">php</option>
                        <option value="powershell">powershell</option>
                        <option value="prolog">prolog</option>
                        <option value="properties">properties</option>
                        <option value="python">python</option>
                        <option value="r">r</option>
                        <option value="rdoc">rdoc</option>
                        <option value="rhtml">rhtml</option>
                        <option value="ruby">ruby</option>
                        <option value="rust">rust</option>
                        <option value="sass">sass</option>
                        <option value="scad">scad</option>
                        <option value="scala">scala</option>
                        <option value="scheme">scheme</option>
                        <option value="scss">scss</option>
                        <option value="sh">sh</option>
                        <option value="snippets">snippets</option>
                        <option value="sql">sql</option>
                        <option value="stylus">stylus</option>
                        <option value="svg">svg</option>
                        <option value="tcl">tcl</option>
                        <option value="tex">tex</option>
                        <option value="text">text</option>
                        <option value="textile">textile</option>
                        <option value="toml">toml</option>
                        <option value="twig">twig</option>
                        <option value="typescript">typescript</option>
                        <option value="vbscript">vbscript</option>
                        <option value="velocity">velocity</option>
                        <option value="verilog">verilog</option>
                        <option value="xml">xml</option>
                        <option value="xquery">xquery</option>
                        <option value="yaml">yaml</option>
                    </select>
                    <select id="codeTheme" size="1">
                        <optgroup label="bright">
                            <option value="chrome">chrome</option>
                            <option value="clouds">clouds</option>
                            <option value="crimson_editor">crimson_editor</option>
                            <option value="dawn">dawn</option>
                            <option value="dreamweaver">dreamweaver</option>
                            <option value="eclipse">eclipse</option>
                            <option value="github">github</option>
                            <option value="solarized_light">solarized_light</option>
                            <option value="textmate">textmate</option>
                            <option value="tomorrow">tomorrow</option>
                            <option value="xcode">xcode</option>
                        </optgroup>
                        <optgroup label="dark">
                            <option value="clouds_midnight">clouds_midnight</option>
                            <option value="cobalt">cobalt</option>
                            <option value="idle_fingers">idle_fingers</option>
                            <option value="kr_theme">kr_theme</option>
                            <option value="merbivore">merbivore</option>
                            <option value="merbivore_soft">merbivore_soft</option>
                            <option value="mono_industrial">mono_industrial</option>
                            <option value="monokai" selected>monokai</option>
                            <option value="pastel_on_dark">pastel_on_dark</option>
                            <option value="solarized_dark">solarized_dark</option>
                            <option value="terminal">terminal</option>
                            <option value="tomorrow_night">tomorrow_night</option>
                            <option value="tomorrow_night_blue">tomorrow_night_blue</option>
                            <option value="tomorrow_night_bright">tomorrow_night_bright</option>
                            <option value="tomorrow_night_eighties">tomorrow_night_eighties</option>
                            <option value="twilight">twilight</option>
                            <option value="vibrant_ink">vibrant_ink</option>
                        </optgroup>
                    </select>
                    <div form="addNewSnippetForm" id="editor"></div>
                </form>
            </div>

            <div class="snippetsListView activeCodeView">
                <button id="addNewSnippetViewButton">Add new snippet</button>
                <ul id="snippetList">
                    <!--
                            CODE SNIPPETS
                     -->
                </ul>
            </div>

            <div id="snippetPreviewView">
                <button id="snippetPreviewGoBack">Go back</button>
                <h3 id="snippetPreviewTitle"></h3>
                <div id="editor-preview"></div>
            </div>
        </div>

        <div id="sidebar">
            <p>Online</p>
            <ul id="users_online">
                <!--
                        USERS
                 -->
            </ul>
        </div>

        <ul id="chat-content">
            <!--
                    MESSAGES
             -->
        </ul>

        <div id="login" class="login_style">
            <h3>Login</h3>
            <div id="login-wrapper2">
                <form id="form_login" action="">
                    <label for="login_username"></label>
                    <input id="login_username" placeholder="Username" minlength="2" maxlength="15" form="form_login" autofocus>
                    <input id="login_password" placeholder="Password" form="form_login">
                    <button form="form_login">Connect</button>
                </form>
            </div>
        </div>

        <div id="message-wrapper">
            <form id="form_send_message" action="">
                <label for="submit_message"></label>
                <input id="submit_message" autocomplete="off" autofocus>
            </form>
        </div>
    </div>

    <audio id="boop" src="/p/sounds/pop.mp3" preload="auto"></audio>
</body>
</html>