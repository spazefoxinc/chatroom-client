<?php
/**
 * Created by PhpStorm.
 * User: TT-Vincent
 * Date: 4/11/2016
 * Time: 3:59 PM
 */

include "Router.php";

Router::route('/', function(){
    header('location: /p/index.php');
});

Router::route('/admin', function(){
    header('location: /p/admin.php');
});

Router::route('/sl-viewer', function(){
    header('location: /p/sl-viewer.php');
});

Router::route('/mobile', function(){
    header('location: /p/mobile.php');
});

Router::execute($_SERVER['REQUEST_URI']);

/*
function curPageURL()
{
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    }
    else
    {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}
*/